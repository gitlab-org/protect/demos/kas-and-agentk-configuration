# KAS and agentk configuration

This is a [Kubernetes Agent configuration repository](https://docs.gitlab.com/ee/user/clusters/agent/repository.html).

It contains:

1. configuration for `agentk`
1. `agentk`-managed project manifests
1. helm charts for applications installed on the demo cluster


## Installing projects

I need to work that out next.

## Installing applications

Once upon a time, people used [GMAv2](https://docs.gitlab.com/ee/user/clusters/applications.html) for this, which has since been deprecated.

The new recommended way is to use a [cluster management template](https://docs.gitlab.com/ee/user/clusters/management_project_template.html). This, however, suffers from the same reason GMAv2 wasn't adopted by some customers: it requires `cluster-admin` privileges.

The alternative is to become a K8S boss, and install and manage everything ourselves. Which is what we're going to do because it's what customers are more likely to do.

### Dependencies

You need some stuff on your local machine before you can install.

1. Install helm
   ```sh
   brew install helm helmfile
   helm plugin install https://github.com/databus23/helm-diff
   ```
1. Install kubectl
1. Configure kubectl to talk to the target cluster

### Install/update applications

Go into the application directory and run: `helmfile --file helmfile.yaml apply`.

E.g.:

```sh
cd applications/cilium
helmfile --file helmfile.yaml apply
```
